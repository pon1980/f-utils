namespace Pontech.Utils

open System
open System.IO
open System.Diagnostics

module Logger =
  type Level = DEBUG | INFO | WARN | ERROR | CRITICAL | NONE
  type Facility =
    | StdErr
    | StdOut
    | File of StreamWriter
    | Combined of Facility * Facility

  let mutable private levelMask = [DEBUG; INFO; WARN; ERROR; CRITICAL]

  let setLogLevel = function
    | DEBUG -> levelMask <- [DEBUG; INFO; WARN; ERROR; CRITICAL]
    | INFO -> levelMask <- [INFO; WARN; ERROR; CRITICAL]
    | WARN -> levelMask <- [WARN; ERROR; CRITICAL]
    | ERROR -> levelMask <- [ERROR; CRITICAL]
    | CRITICAL -> levelMask <- [CRITICAL]
    | NONE -> levelMask <- []

  let getLogLevel () =
    match levelMask with
    | [] -> NONE
    | x::xs -> x

  let levelToString = function
    | DEBUG -> "DEBUG"
    | INFO -> "INFO"
    | WARN -> "WARN"
    | ERROR -> "ERROR"
    | CRITICAL -> "CRITICAL"
    | NONE -> "NONE"

  let shouldLog level = List.contains level levelMask

  let rec writeToFacility facility (str : string) =
    match facility with
    | StdErr -> Console.Error.Write str
    | StdOut -> Console.Out.Write str
    | File sw -> sw.Write str
    | Combined (f1, f2) -> (writeToFacility f1 str; writeToFacility f2 str)

  let combine f1 f2 = Combined (f1, f2)

  let getCallerFrame (trace : StackTrace) = 
    trace.GetFrames() 
    |> Seq.ofArray
    |> Seq.filter 
      (fun x ->
        let fname = x.GetMethod().Name
        fname.Substring(0, 4) <> "logp")
    |> Seq.head


  let logp rep level facility spec datum =
    if shouldLog level then
      let timestamp = DateTime.Now.ToString "yyyy-MM-dd HH:mm:ss.fff"
      let levelstr = levelToString level
      let specstr = spec |> List.map (fun x -> "[" + x + "]") |> String.concat ""
      let _ = Printf.sprintf "[%s][%s]%s %s\n" timestamp levelstr specstr (rep datum) |> writeToFacility facility
      datum
    else
      datum

  module Generic =
    let logp level facility spec datum = logp (fun x -> Printf.sprintf "%O" x) level facility spec datum
    let logpDebug facility spec datum = logp DEBUG facility spec datum
    let logpInfo facility spec datum = logp INFO facility spec datum
    let logpWarn facility spec datum = logp WARN facility spec datum
    let logpError facility spec datum = logp ERROR facility spec datum
    let logpCritical facility spec datum = logp CRITICAL facility spec datum

    module StdErr =
      let logp level = logp level StdErr
      let logpDebug spec datum = logp DEBUG spec datum
      let logpInfo spec datum = logp INFO spec datum
      let logpWarn spec datum = logp WARN spec datum
      let logpError spec datum = logp ERROR spec datum
      let logpCritical spec datum = logp CRITICAL spec datum

    module StdOut =
      let logp level = logp level StdOut
      let logpDebug spec datum = logp DEBUG spec datum
      let logpInfo spec datum = logp INFO spec datum
      let logpWarn spec datum = logp WARN spec datum
      let logpError spec datum = logp ERROR spec datum
      let logpCritical spec datum = logp CRITICAL spec datum

    module File =
      exception LogFileNotOpened
      let mutable private writer = None
      let getFacility () = match writer with Some wr -> File wr | None -> raise LogFileNotOpened
      let openLog (fn : string) = writer <- Some (new StreamWriter(fn))
      let closeLog () =
        match writer with
        | None -> ()
        | Some x -> (x.Close (); writer <- None)
      let logp level =
        match writer with
        | None -> raise LogFileNotOpened
        | Some w -> logp level (File w)
      let logpDebug spec datum = logp DEBUG spec datum
      let logpInfo spec datum = logp INFO spec datum
      let logpWarn spec datum = logp WARN spec datum
      let logpError spec datum = logp ERROR spec datum
      let logpCritical spec datum = logp CRITICAL spec datum

